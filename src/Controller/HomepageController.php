<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Asset\Package;
use Symfony\Component\Asset\VersionStrategy\EmptyVersionStrategy;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\UX\Cropperjs\Factory\CropperInterface;
use Symfony\UX\Cropperjs\Form\CropperType;

class HomepageController extends AbstractController
{
    #[Route('/homepage', name: 'homepage')]
    public function index( CropperInterface $cropper, Request $request): Response
    {

        $crop = $cropper->createCrop('logo.png');
        $crop->setCroppedMaxSize(2000, 1500);

        $form = $this->createFormBuilder(['crop' => $crop])
            ->add('crop', CropperType::class, [
                'public_url' => "logo.png",
                'aspect_ratio' => 2000 / 1500,
            ])
            ->add('coucou', TextareaType::class, [
            ])
            ->getForm();
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            // Get the cropped image data (as a string)
            $crop->getCroppedImage();

            // Create a thumbnail of the cropped image (as a string)
            $crop->getCroppedThumbnail(200, 150);

            // ...
        }


        return $this->render('homepage/index.html.twig', [
            'controller_name' => 'HomepageController',
            'form' => $form->createView(),
        ]);
    }
}
